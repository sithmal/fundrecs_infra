# Fund-Recs Infrastructure Provisioning
## Fund-Recs Assignment submission
 - Create Cloud Formation YAML for an ECS cluster with 2 services running springboot apps.
 - The services will need to be publicly accessible. 
 - The cluster should be configured securely.
 - The service code will be stored in CodeCommit. 
 - When code is pushed to master, CodeBuild will build and push to docker and update the ECS service.
 - For spring boot services, any simple example can be used.


## TODO

- Deploy private service in private subnet and access through internal load balancer.
- Create Route53 zone for access via domain.
- Hardening IAM roles and SG/NACL for secure the setup.
- Refactor naming conventions in resources.
- Optimizing the code to re-use components with variablizing Ex. Service deployment. 
- Create proper documentation with network layout.


## Intro

This is simple structure with the public facing load-balancer with private subnets for the ECS services. 

## Usage
- You can fork the repo, configure aws credentials on the GitLab CI/CD variables.
- Access via the public loadbalancer with service port : Ex: (public-lb-domain:8080)

## Installation
- Create S3 bucket for First time code commit initialization and add the [sample spring boot project](https://gitlab.com/sithmal/fundrecs_infra/-/blob/main/fundrecs_login.zip). Ex. fundrecs-code-bucket.
  Or else you can override the parameter 'CodeBucket' of the application_dep.yml with the existing S3.
- Export variables : 

    **PROJECT_PREFIX=fundrecs**

    **ENV=dev**

- vpc.yml - VPC with 2 private subnets, 2 public subnets, 2 public and private Loadbalancers and dummy targets groups for initialize the LBs.
    
    *aws cloudformation deploy --stack-name $PROJECT_PREFIX-$ENV-vpc --capabilities CAPABILITY_IAM --template-file ./vpc.yml*

- ecs.yml - ECS Cluster creation.

    *aws cloudformation deploy --stack-name $PROJECT_PREFIX-$ENV-ecs --capabilities CAPABILITY_IAM --template-file ./ecs.yml*
  

- application_dep.yml - Application Dependencies - CodeCommit and CodeBuild

    *aws cloudformation deploy --stack-name $PROJECT_PREFIX-$ENV-app-dep --parameter-overrides CodeBucket=fundrecs-code-bucket CodeKey=**fundrecs_login.zip** --capabilities CAPABILITY_NAMED_IAM --template-file ./application_dep.yml*

- service_dep.yml - ECS Taskdefintion creation and Service creation.

    *aws cloudformation deploy --stack-name $PROJECT_PREFIX-$ENV-svc-dep --parameter-overrides VPCStackName=$PROJECT_PREFIX-$ENV-vpc ECSStackName=$PROJECT_PREFIX-$ENV-ecs --capabilities CAPABILITY_NAMED_IAM --template-file ./service_dep.yml*

- code_pipeline.yml - CodePipeline Creation - This should be run at last. 

    *aws cloudformation deploy --stack-name fundrecs-dev-pipeline-dep --parameter-overrides ECSStackName=fundrecs-dev-ecs --capabilities CAPABILITY_NAMED_IAM --template-file ./code_pipeline.yml*

## Project status
    WIP

## References
- VPC / ECS : https://github.com/awslabs/aws-cloudformation-templates 
- CodeBuild : https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-codebuild-project-environment.html,
https://github.com/aws-samples/aws-codebuild-samples/blob/master/cloudformation/continuous-deployment.yml
- ECS Tasks and Services: https://github.com/1Strategy/fargate-cloudformation-example/blob/master/fargate.yaml, 

- CodePiple :
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-codepipeline-pipeline-artifactstore.html
https://github.com/amazon-archives/ecs-mxnet-example/blob/master/template/cicd.yaml ,
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/quickref-s3.html

- https://www.json2yaml.com/


## NOTE
**When deleting stack you have empty the codepipeline S3 bucket and the Images from the ECR Repo.**
